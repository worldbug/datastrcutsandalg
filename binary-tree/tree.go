package main

import "errors"

type node struct {
	left  *node
	value int
	right *node
}

func addToNode(newValue int, master *node) {
	for true {
		if master.value == 0 {
			// delete it
			master.value = newValue
		} else if master.value < newValue {
			if master.right != nil {
				master = master.right
				continue
			}
			master.right = &node{value: newValue}
			break

		} else if master.value == newValue {
			// execpt
			break
		} else if master.value > newValue {
			if master.left != nil {
				master = master.left
				continue
			}
			master.left = &node{value: newValue}
			break
		}
	}
}

func getValue(value int, master *node) (int, error) {
	var c int
	for true {
		c++
		if master.value == value {
			break
		} else if master.value > value {
			if master.left != nil {
				master = master.left
				continue
			}
			return 0, errors.New("No value")
		} else if master.value < value {
			if master.right != nil {
				master = master.right
				continue
			}
			return 0, errors.New("No value")
		}
	}
	return c, nil
}
